data {
  int <lower = 0> N_points;
  vector [N_points] x;
  int <lower = 2> N_components;
}

parameters {
  simplex [N_components] mix_weight;
  positive_ordered [N_components] gamma_mean;
  vector <lower = 0> [N_components] gamma_var;
}

transformed parameters {
  vector [N_components] gamma_shape;
  vector [N_components] gamma_rate;

  for (ii in 1 : N_components) {
    gamma_shape[ii] = (gamma_mean[ii]^2) / gamma_var[ii];
    gamma_rate[ii] = gamma_mean[ii] / gamma_var[ii];
  }
}

model {
  for (ii in 1 : N_points) {
    vector [N_components] contribs;

    for (jj in 1 : N_components) {
      contribs[jj] = log(mix_weight[jj]) +
        gamma_lpdf(x[ii] | gamma_shape[jj], gamma_rate[jj]);
    }

    target += log_sum_exp(contribs);
  }

  target += dirichlet_lpdf(mix_weight | rep_vector(1.5, N_components));
  target += normal_lpdf(gamma_mean | {50, 120.0, 170}, 20.0);
  target += normal_lpdf(gamma_var | 0.0, 150.0);
}
