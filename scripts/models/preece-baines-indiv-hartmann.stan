functions {
  vector pb_model (vector times, real h0, real h1, real s0, real s1, real theta) {
    int N_obs = num_elements(times);
    vector [N_obs] result;

    for (ii in 1 : N_obs) {
      real denom_one = exp(s0 * (times[ii] - theta));
      real denom_two = exp(s1 * (times[ii] - theta));
      real numerator = 2 * (h1 - h0);
      result[ii] = h1 - (numerator / (denom_one + denom_two));
    }

    return(result);
  }
}

data {
  int <lower = 1> N_obs_per_indiv;
  vector <lower = 1, upper = 20> [N_obs_per_indiv] obs_times;
  vector <lower = 0, upper = 250> [N_obs_per_indiv] heights;

  real h0_lognorm_location;
  real <lower = 0> h0_lognorm_scale;

  real h1_lognorm_location;
  real <lower = 0> h1_lognorm_scale;

  real s0_lognorm_location;
  real <lower = 0> s0_lognorm_scale;

  real s1_lognorm_location;
  real <lower = 0> s1_lognorm_scale;

  real theta_lognorm_location;
  real <lower = 0> theta_lognorm_scale;
}

transformed data {
  real <lower = 0> min_obs_time = min(obs_times);
  real <lower = 0> max_obs_time = max(obs_times);
}

parameters {
  real <lower = 0> noise_sd;
  positive_ordered [2] hs;
  positive_ordered [2] ss;
  real <lower = min_obs_time, upper = max_obs_time> theta;
}

model {
  vector [N_obs_per_indiv] mu = pb_model(
    obs_times,
    hs[1],
    hs[2],
    ss[1],
    ss[2],
    theta
  );

  target += normal_lpdf(heights | mu, noise_sd);
  target += lognormal_lpdf(noise_sd | log(1), 0.2);

  target += lognormal_lpdf(hs[1] | h0_lognorm_location, h0_lognorm_scale);
  target += lognormal_lpdf(hs[2] | h1_lognorm_location, h1_lognorm_scale);
  target += lognormal_lpdf(ss[1] | s0_lognorm_location, s0_lognorm_scale);
  target += lognormal_lpdf(ss[2] | s1_lognorm_location, s1_lognorm_scale);
  target += lognormal_lpdf(theta | theta_lognorm_location, theta_lognorm_scale);
}


generated quantities {
  real <lower = 0> h0 = hs[1];
  real <lower = 0> delta_h1 = hs[2] - hs[1];
  real <lower = 0> s0 = ss[1];
  real <lower = 0> delta_s1 = ss[2] - ss[1];
}
