data {
  real b;

  real h0;
  real h1;
  real s0;
  real s1;
  real gamma_mid;

  int n_times;
  vector [n_times] times;
}

parameters {
  real <lower = 0> y [n_times];
}

transformed parameters {
  real model_mean [n_times];
  real log_log_vals [n_times];

  for (ii in 1 : n_times) {
    model_mean[ii] = h1 - (
      2 * (h1 - h0) / 
      (exp(s1 * (times[ii] - gamma_mid)) + exp(s0 * (times[ii] - gamma_mid)))
    );

    log_log_vals[ii] = b * (log(y[ii]) + lgamma(1 + (1 / b)) - model_mean[ii]);

    print("ii: ", ii);
    print("model_mean: ", model_mean[ii]);
    print("log_log_vals: ", log_log_vals[ii]);
  }
}

model {
  for (ii in 1 : n_times) {
    real t1 = (b - 1) * log(y[ii]);
    real t2 = -exp(log_log_vals[ii]);

    print("ii: ", ii);
    print("t1: ", t1);
    print("t2: ", t2);

    target += t1;
    target += t2;
  }
}
