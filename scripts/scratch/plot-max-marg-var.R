library(dplyr)
library(ggplot2)

source("scripts/preece-baines-growth/GLOBALS.R")

res_files <- Sys.glob("rds/scratch/max-marg-var-dual/*.rds")
results <- lapply(res_files, \(x) readRDS(x))

# maybe plot a line for each rep
# the kappa here defines the utility function
# smaller kappa mean we value the log(mean(sds)) term less
kappa <- 1
pareto_front_tbl <- lapply(1 : length(results), function(rep_id) {
  results[[rep_id]]$pareto.front %>% 
    as_tibble() %>% 
    arrange(y_1, y_2) %>%
    mutate(utility = -(y_1 + kappa * y_2), rep_id = rep_id)
}) %>%
  bind_rows()

ggplot(pareto_front_tbl, aes(x = y_1, y = y_2, colour = utility, group = rep_id)) +
  geom_line()

opt_samples <- lapply(1 : length(results), function(rep_id) {
  local_mlr_res <- results[[rep_id]]
  local_y_tbl <- local_mlr_res$pareto.front %>%
    as_tibble() %>%
    mutate(utility = -(y_1 + kappa * y_2))
  
  local_max_util_index <- local_y_tbl %>%
    pull(utility) %>%
    which.max()
  
  local_samples <- local_mlr_res$pareto.set[[local_max_util_index]] %>%
    unlist() %>%
    prior_marginal_sampler(n = 500)

  local_samples <- local_samples %>%
    mutate(
      rep_id = rep_id, 
      objective_function = local_y_tbl$utility[local_max_util_index]
    ) %>%
    filter(param != "norm_noise_sd")

  return(local_samples)
}) %>%
  bind_rows()

# min_short_run <- opt_samples %>%
#   distinct(rep_id, objective_function) %>%
#   arrange(objective_function) %>%
#   slice_head(n = 1) %>%
#   pull(rep_id)
# 
# best_half <- opt_samples %>%
#   pull(objective_function) %>%
#   unique() %>%
#   sort() %>%
#   quantile(0.025)
# 
# best_runs <- opt_samples %>%
#   distinct(rep_id, objective_function) %>%
#   filter(objective_function < best_half) %>%
#   pull(rep_id)


limits_list <- list(
  delta_h1 = list(
    x = scale_x_continuous(limits = c(0, 50)),
    y = scale_y_continuous(limits = c(0, NA))
  ),
  delta_s1 = list(
    x = scale_x_continuous(limits = c(0, 1)),
    y = scale_y_continuous(limits = c(0, 1))
  ),
  h0 = list(
    x = scale_x_continuous(limits = c(NA, NA)),
    y = scale_y_continuous(limits = c(0, NA))
  ),
  s0 = list(
    x = scale_x_continuous(limits = c(0, 1)),
    y = scale_y_continuous(limits = c(0, 1))
  ),
  theta = list(
    x = scale_x_continuous(limits = c(8, 18)),
    y = scale_y_continuous(limits = c(0, 1))
  )
)

plot_list <- list()

for (param in unique(opt_samples$param)) {
  plot_list[[param]] <- ggplot(
    opt_samples %>% 
      filter(param == !!param),
    aes(x = x, group = rep_id, colour = objective_function)
  ) +
    geom_line(
      stat = "density",
      alpha = 0.8
    ) +
    limits_list[[param]][["x"]] +
    limits_list[[param]][["y"]]
}

plot_list

# prior_pred_at_best <- lapply(ages_of_interest, function(an_age) {
#   res_list <- list()
#   for (ii in 1 : length(best_runs)) {
#     res_list[[ii]] <- tibble(
#       x = results[[best_runs[ii]]]$x %>%
#         unlist() %>%
#         full_prior_predictive_sampler_cov(n = 1e3, lambda = ., cov_val = an_age),
#       age = an_age,
#       rep_id = best_runs[ii]
#     )
#   }
#   
#   res <- bind_rows(res_list)
#   return(res)
# }) %>%
#   bind_rows()
# 
# ggplot(prior_pred_at_best, aes(x = x, group = rep_id)) +
#   geom_line(
#     stat = "density"
#   ) +
#   facet_wrap(vars(age), scales = "free") + 
#   scale_x_continuous(limits = c(0, 230))
