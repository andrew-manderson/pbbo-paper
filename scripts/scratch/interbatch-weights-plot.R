log_ds <- runif(n = 50, min = -5, max = 5)

# not enough diff between things spanning 10 orders of magnitude
w_tilde <- -exp(log_ds)
w1 <- w_tilde - min(w_tilde)
w <- w1 / sum(w1)

sort(w)

# current way
w_curr <- exp(w_tilde - matrixStats::logSumExp(w_tilde))
sort(w_curr)

plot(x = 1 : length(w_curr), y = sort(w_curr))
points(x = 1 : length(w_curr), y = sort(w), col = "blue")
