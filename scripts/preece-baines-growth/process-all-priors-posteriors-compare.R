library(dplyr)
library(purrr)
library(ggplot2)
library(parallel)
library(tidyr)
library(pbapply)

source("scripts/common/setup-argparse.R")
source("scripts/preece-baines-growth/GLOBALS.R")

parser$add_argument("--prior-population-optima-results")
parser$add_argument("--prior-covariate-optima-results")
parser$add_argument("--post-pop-optima-samples")
parser$add_argument("--post-cov-optima-samples")
parser$add_argument("--post-flat-any-warnings")
parser$add_argument("--flat-intermediaries-dir")
parser$add_argument("--hartmann-priors")
parser$add_argument("--hartmann-intermediaries-dir")
args <- parser$parse_args()

pop_optima_prior <- readRDS(args$prior_population_optima_results)
cov_optima_prior <- readRDS(args$prior_covariate_optima_results)
pop_posterior <- readRDS(args$post_pop_optima_samples)
cov_posterior <- readRDS(args$post_cov_optima_samples)
flat_warnings <- readRDS(args$post_flat_any_warnings)
flat_intermediaries <- args$flat_intermediaries_dir
hartmann_priors <- readRDS(args$hartmann_priors)
hartmann_post_all <- Sys.glob(
  file.path(args$hartmann_intermediaries_dir, "user-*.rds")
) %>%
  lapply(function(x) readRDS(x))

params_of_interest <- c("h0", "delta_h1", "s0", "delta_s1", "theta")
draws_per_rep <- 2000

hart_row_to_draws <- function(tbl, n_draws) {
  res <- tibble(
    x = rlnorm(
      n = n_draws,
      meanlog = tbl$lognorm_location,
      sdlog = tbl$lognorm_scale
    ),
    param = tbl$param,
    user_id = tbl$user_id
  )
}

hart_draws <- lapply(1 : nrow(hartmann_priors), function(row_id) {
  hart_row_to_draws(hartmann_priors[row_id, ], draws_per_rep)
}) %>%
  bind_rows() %>%
  filter(param != "weib_noise") %>%
  mutate(
    sample_id = rep(
      1 : draws_per_rep,
      times = length(params_of_interest) * n_distinct(user_id)
    )
  ) %>%
  pivot_wider(names_from = param, values_from = x) %>%
  mutate(
    delta_h1 = h1 - h0,
    delta_s1 = s1 - s0
  ) %>%
  select(-c(h1, s1)) %>%
  pivot_longer(
    cols = all_of(params_of_interest),
    names_to = "param",
    values_to = "x"
  ) %>%
  mutate(dist_type = "prior", origin = "hartmann")

flat_mean_warnings <- flat_warnings %>%
  group_by(indiv_id) %>%
  summarise(mean_warnings = mean(x))

## median
# median_indiv_val <- flat_mean_warnings %>%
#   pull(mean_warnings) %>%
#   median()
#
# median_indiv_index <- which(flat_mean_warnings$mean_warnings == median_indiv_val) %>%
#   head(n = 1)
#
# median_indiv_id <- flat_mean_warnings %>%
#   filter(indiv_id == median_indiv_index) %>%
#  pull(indiv_id)

## worst
worst_indiv_id <- flat_mean_warnings %>%
  ungroup() %>%
  slice_max(mean_warnings) %>%
  pull(indiv_id)

## best
# median_indiv_under_flat <- flat_mean_warnings %>%
#   ungroup() %>%
#   slice_min(mean_warnings) %>%
#   pull(indiv_id)

all_worst_runs <- flat_warnings %>%
  filter(indiv_id == worst_indiv_id)

hart_posterior <- lapply(hartmann_post_all, function(x) {
  x[["par_samples"]] %>%
    filter(
      indiv_id == worst_indiv_id,
      .variable %in% params_of_interest,
      .chain == 2,
      .iteration <= draws_per_rep
    ) %>%
    select(-indiv_id) %>%
    select(
      x = .value,
      param = .variable,
      rep_id = user_id
    ) %>%
    mutate(origin = "hartmann", dist_type = "posterior")
}) %>%
  bind_rows()

# somewhat tempting to pre-process this, but the resulting file would take just
# as long to read in.
flat_posterior <- pblapply(sort(all_worst_runs$rep_id), cl = 5, function(a_rep_id) {
  f_to_read <- file.path(flat_intermediaries, paste0("run-", a_rep_id, ".rds"))
  run_res <- readRDS(f_to_read)
  res <- run_res$par_samples %>%
    filter(
      .chain == 3,
      .iteration <= draws_per_rep,
      indiv_id == worst_indiv_id,
      .variable != "noise_sd"
    ) %>%
    select(param = .variable, x = .value) %>%
    mutate(dist_type = "posterior", origin = "flat", rep_id = a_rep_id)
}) %>%
  bind_rows()

posteriors <- bind_rows(
  flat_posterior,
  hart_posterior,
  pop_posterior %>%
    filter(
      indiv_id == worst_indiv_id,
      .variable != "noise_sd",
      .iteration <= draws_per_rep
    ) %>%
    mutate(
      dist_type = "posterior",
      kappa_str = ifelse(is.na(kappa_val), "NA", as.character(kappa_val)),
      origin = sprintf("'Cov-indep.' ~ italic(kappa) == '%s'", kappa_str)
    ) %>%
    select(x = .value, param = .variable, rep_id = run_id, dist_type, origin),
  cov_posterior %>%
    filter(
      indiv_id == worst_indiv_id,
      .variable != "noise_sd",
      .iteration <= draws_per_rep
    ) %>%
    mutate(
      dist_type = "posterior",
      kappa_str = ifelse(is.na(kappa_val), "NA", as.character(kappa_val)),
      origin = sprintf("'Cov-specific.' ~ italic(kappa) == '%s'", kappa_str)
    ) %>%
    select(x = .value, param = .variable, rep_id = run_id, dist_type, origin)
)

# the priors need sampling -- i don't store draws from them (maybe i should?)
optima_sampler <- function(tbl) {
  reps <- unique(tbl$rep_id)

  lapply(reps, function(inner_rep_id) {
    local_pars <- tbl %>%
      filter(rep_id == inner_rep_id)

    local_kappa <- unique(local_pars$kappa)

    lambda <- local_pars %>%
      pull(param_opt_value)

    names(lambda) <- local_pars %>%
      pull(param)

    res <- lambda %>%
      prior_marginal_sampler(n = draws_per_rep, lambda = .) %>%
      mutate(
        rep_id = inner_rep_id,
        kappa_str = ifelse(is.na(local_kappa), "NA", as.character(local_kappa))
      )
  }) %>%
    bind_rows()
}

priors <- bind_rows(
  hart_draws %>%
    select(-sample_id, rep_id = user_id),
  pop_optima_prior %>%
    optima_sampler() %>%
    filter(param != "norm_noise_sd") %>%
    mutate(
      dist_type = "prior",
      origin = sprintf("'Cov-indep.' ~ italic(kappa) == '%s'", kappa_str)
    ),
  cov_optima_prior %>%
    optima_sampler() %>%
    filter(param != "norm_noise_sd") %>%
    mutate(
      dist_type = "prior",
      origin = sprintf("'Cov-specific.' ~ italic(kappa) == '%s'", kappa_str)
    )
)

full_draws <- bind_rows(priors, posteriors)
saveRDS(file = args$output, object = full_draws)
