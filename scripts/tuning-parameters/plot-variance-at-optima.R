library(ggplot2)
library(dplyr)

source("scripts/common/setup-ggplot-theme.R")
source("scripts/common/setup-argparse.R")

parser$add_argument("--variance-at-optima-tbl")
args <- parser$parse_args()

plot_tbl <- readRDS(args$variance_at_optima_tbl)
ref_val <- plot_tbl %>%
  filter(prior_samples == 1e5, importance_samples == 1e4 + 1) %>%
  pull(x) %>%
  mean()

p1 <- ggplot(data = plot_tbl) +
  geom_histogram(mapping = aes(x = x, y = after_stat(density)), bins = 27) +
  facet_grid(rows = vars(prior_samples), cols = vars(importance_samples)) +
  geom_segment(
    mapping = aes(x = ref_val, xend = ref_val, y = 0, yend = 4.6),
    colour = highlight_col,
    linetype = "solid",
    alpha = 0.3
  ) +
  ylab("Density") +
  xlab(expression("log"(italic("D") * (italic(lambda)^{"*"}))))

ggsave_fullpage(
  plot = p1,
  filename = args$output
)
