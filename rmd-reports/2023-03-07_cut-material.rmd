---
title: "Cut material"
author: "Andrew Manderson"
date: "`r format(Sys.time(), '%d %B, %Y')`"
fontfamily: tgpagella
fontsize: 12pt
papersize: a4
geometry: margin=2.25cm
bibliography: ../bibliography/prior-setting.bib
csl: ../bibliography/journal-of-the-royal-statistical-society.csl
output:
  pdf_document:
    includes:
      in_header:
        ../tex-input/pre.tex
    fig_caption: true
    number_sections: true
    keep_tex: true
    citation_package: natbib
urlcolor: blue
---
<!--
Rscript -e "rmarkdown::render('rmd-reports/')"
-->

# literature review

<!-- cut for paper -->
<!-- Principle-based priors are regularly encountered when seeking methods addressing the issue of prior specification.
Invariance to reparameterisation (Jefferys prior) is the prototypical example [see @kass_selection_1996 for an extensive discussion of other principles], but yields "uninformative" priors.
If used with a small number of observations, or a complex model, reference priors do not constrain the posterior/model behaviour to something physically plausible, and almost never reflect the knowledge we have of the phenomena.
Penalised complexity priors [@simpson_penalising_2017, who also review many other principle-based methods for prior specification] aim to embed parsimony into prior specification, so that the model is only as flexible  as necessary.
The penalised complexity prior is thoughtfully constructed to allow a modeller to, via one meaningful tuning parameter, increase the flexibility of the model from some _base_, simple model toward a more complex one.
An interesting, predictive extension to this idea is presented in @nalisnick_predictive_2021, who employ mathematical machinery akin to @simpson_penalising_2017, but do so using the prior predictive distribution in place of the prior. -->

# pbbo demonstration -- tuning parameters

<!-- ## A simple example `pbbo` demonstration

Suppose the target distribution $\tc(Y)$ for the observable quantity $Y$ is the $\text{N}(2, 0.5^2)$ CDF. 
To use `pbbo` we must define functions for the evaluating the target log-CDF drawing samples from the target:

```{r, eval = FALSE, echo = TRUE}
library(pbbo); library(ParamHelpers)

target_lcdf <- function(x) pnorm(x, mean = 2, sd = 0.5, log.p = TRUE)
target_sampler <- function(n) rnorm(n = n, mean = 2, sd = 0.5)
```

We must also define a function that samples the prior predictive distribution for our model, given its hyperparameters, $\pd(Y \mid \lambda)$.
In this simple example we define the model to exactly coincide with the target.
That is, the model $\pd(Y \mid \lambda)$ is defined to be $Y \sim \text{N}(\mu, \sigma^{2})$ with $\lambda = (\mu, \sigma)$.
We set the support for $\mu$ to be $[-50, 50]$ and for $\sigma$ it is $[0, 20]$.
The components of $\lambda$ and corresponding support $\Lambda$ must be enumerated using `makeParamSet` from the `ParamHelpers` package [@bischl_paramhelpers_2022]:

```{r, eval = FALSE, echo = TRUE}
prior_predictive_sampler <- function(n, lambda) {
  rnorm(n = n, mean = lambda["mu"], sd = lambda["sigma"])
}

param_set <- makeParamSet(
  makeNumericParam(id = "mu", lower = -50, upper = 50),
  makeNumericParam(id = "sigma", lower = 0, upper = 20)
)
```

We can then call `pbbo` to estimate an optimal value of $\lambda = (\mu, \sigma)$ by minimising the Cramér-Von Mises discrepancy between $\tc(Y)$ and $\Pd(Y \mid \lambda)$ (we explain the purpose of the tuning parameters and appropriate values for them in the following section):

```{r, eval = FALSE, echo = TRUE}
pbbo_res <- suppressWarnings(pbbo(
  target_lcdf = target_lcdf,
  target_sampler = target_sampler,
  prior_predictive_sampler = prior_predictive_sampler,
  param_set = param_set,
  n_crs2_iters = 300,
  n_internal_prior_draws = 5e3,
  n_internal_importance_draws = 1e3,
  importance_method = "uniform",
  bayes_opt_batches = 1,
  bayes_opt_iters_per_batch = 50,
  bayes_opt_design_points_per_batch = 40
))
#> INFO [2022-07-22 16:57:35] Starting stage one CRS2 optimiser
#> INFO [2022-07-22 16:57:42] Starting Bayes opt batch 1
opt_lambda <- get_best_lambda(pbbo_res); print(opt_lambda)
```

```{r}
print(readRDS("rds/readme/simple-lambda-est.rds"))
```
Unsurprisingly, in this contrived example our estimate of the model hyperparameters closely matches the definition of $\tc(Y)$. 
This is an artefact of the simplicity of the example, as most prior predictive distributions will not be of the same mathematical form as the target distribution.
By visually comparing the prior predictive distribution at the optima $\pd(Y \mid \lambda^{*})$ against the target $\tp(Y)$, we make the preciseness of this match obvious:

```{r, eval = FALSE, echo = TRUE}
library(ggplot2); library(dplyr); library(tibble); n_plot_samples <- 5e4

plot_tbl <- tibble(
  x = c(
    prior_predictive_sampler(n_plot_samples, opt_lambda),
    target_sampler(n_plot_samples)
  ),
  grp = rep(c("optima", "target"), each = n_plot_samples)
)

ggplot(plot_tbl) +
  geom_line(mapping = aes(x = x, colour = grp), stat = "density") +
  labs(colour = "Type")
```

```{r}
knitr::include_graphics("plots/readme/simple-obs-plot.pdf")
```

## A more complex, multi-objective example 

Suppose now we have an under-specified problem. 
In these settings we require additional criteria to provide a form of regularisation.
Consider the same target $\tc(Y)$ as the previous example:

```{r, eval = FALSE, echo = TRUE}
library(pbbo); library(ParamHelpers)

target_lcdf <- function(x) pnorm(x, mean = 2, sd = 0.5, log.p = TRUE)
target_sampler <- function(n) rnorm(n = n, mean = 2, sd = 0.5)
```

Now instead consider a hierarchical model for the observable $Y$, where $Y \sim \text{N}(\mu, \sigma^{2})$; with parameters $\mu \sim \text{N}(l_{\text{pop}}, s_{\text{pop}}^2)$, $\sigma \sim \text{Gamma}(\alpha_{\text{noise}}, \beta_{\text{noise}})$ such that $\theta = (\mu, \sigma)$; and $\lambda = (l_{\text{pop}}, s_{\text{pop}}, \alpha_{\text{noise}}, \beta_{\text{noise}})$.

```{r, eval = FALSE, echo = TRUE}
hiearchical_prior_predictive_sampler <- function(n, lambda) {
  indiv_mu <- rnorm(n = n, mean = lambda["pop_loc"], sd = lambda["pop_scale"])
  noise_sd <- rgamma(n = n, shape = lambda["noise_shape"], rate = lambda["noise_rate"])
  res <- rnorm(n = n, mean = indiv_mu, sd = noise_sd)
  return(res)
}

# a small numerical epsilon is required to avoid degenerate priors
param_set <- makeParamSet(
  makeNumericParam(id = "pop_loc", lower = -50, upper = 50),
  makeNumericParam(id = "pop_scale", lower = 0 + 1e-6, upper = 20),
  makeNumericParam(id = "noise_shape", lower = 2, upper = 20),
  makeNumericParam(id = "noise_rate", lower = 2, upper = 20)
)
```
Note the careful definition of the upper and lower limits that constitute $\Lambda$.
We do this to avoid numerical degeneracy in the prior predictive distribution.

In this example we can attribute variability in the target to either variation in the population mean, or noise in the measurement process.
This results in an underspecified minimisation problem.
For reference, we naively employ the single objective approach:

```{r, eval = FALSE, echo = TRUE}
pbbo_single_objective_res <- suppressWarnings(pbbo(
  target_lcdf = target_lcdf,
  target_sampler = target_sampler,
  prior_predictive_sampler = hiearchical_prior_predictive_sampler,
  param_set = param_set,
  n_crs2_iters = 300,
  n_internal_prior_draws = 5e3,
  n_internal_importance_draws = 1e3,
  importance_method = "uniform",
  bayes_opt_batches = 1,
  bayes_opt_iters_per_batch = 50,
  bayes_opt_design_points_per_batch = 40
))
#> INFO [2022-07-22 16:58:16] Starting stage one CRS2 optimiser
#> INFO [2022-07-22 16:58:26] Starting Bayes opt batch 1
```

For comparison, we employ the multi-objective approach using the negative mean of the log marginal standard deviations for the parameters, denoted $N(\lambda)$.
We do this using `pbbo`'s optional `extra_objective_term` argument, which we set to be equal to $N(\lambda)$:

```{r, eval = FALSE, echo = TRUE}
neg_mean_log_sd <- function(lambda) {
  raw_sds <- c(
    lambda["pop_scale"], 
    sqrt(lambda["noise_shape"]) / lambda["noise_rate"]
  )

  # neg mean log sd  --- applying inside out from def in Eq (4), so:
  # sds => log sds => take mean => multiply by -1
  res <- raw_sds %>%
    log() %>% mean() %>% magrittr::multiply_by(-1)
}

pbbo_multi_objective_res <- suppressWarnings(pbbo(
  target_lcdf = target_lcdf,
  target_sampler = target_sampler,
  prior_predictive_sampler = hiearchical_prior_predictive_sampler,
  param_set = param_set,
  n_crs2_iters = 300,
  n_internal_prior_draws = 5e3,
  n_internal_importance_draws = 1e3,
  importance_method = "uniform",
  bayes_opt_batches = 1,
  bayes_opt_iters_per_batch = 50,
  bayes_opt_design_points_per_batch = 40,
  extra_objective_term = neg_mean_log_sd
))
#> INFO [2022-07-22 16:59:33] Starting stage one CRS2 optimiser
#> INFO [2022-07-22 16:59:45] Starting Bayes opt batch 1
```

Before inspecting both approaches on the observable scale, we must choose $\kappa$ for the multi-objective approach.
Appropriate choices depend on the relative scale of the objectives, but we note that for our definitions $D(\lambda)$ and $N(\lambda)$ suitable values of $\kappa$ are between $0.2$ and $2$.
The process for obtaining a suitable $\kappa$ will be covered in detail in each forthcoming example.
In this toy example we select $\kappa = 0.75$, and with this we can visually compare the target and the optimal prior predictive distributions:

```{r, eval = FALSE, echo = TRUE}
pal_colours <- scales::hue_pal()(3)

obs_scale_plot_tbl <- tibble(
  x = c(
    target_sampler(n_plot_samples),
    pbbo_single_objective_res %>%
      get_best_lambda() %>%
      hiearchical_prior_predictive_sampler(n = n_plot_samples, lambda = .),
    pbbo_multi_objective_res %>%
      get_best_lambda(pbbo_kappa = 0.75) %>%
      hiearchical_prior_predictive_sampler(n = n_plot_samples, lambda = .)
  ),
  grp = rep(c("target", "single obj", "multi obj"), each = n_plot_samples)
)

ggplot(obs_scale_plot_tbl) +
  geom_line(mapping = aes(x = x, colour = grp), stat = "density") +
  labs(colour = "Type") +
  scale_colour_manual(
    values = c(
      "multi obj" = pal_colours[1],
      "single obj" = pal_colours[2],
      "target" = pal_colours[3]
    )
  )
```

```{r}
knitr::include_graphics("plots/readme/hierarchical-obs.pdf")
```
We see that both methods match the supplied target well, with the multi-objective approach performing slightly better. 
However, when we view the estimated prior for the parameters $\pd(\theta \mid \lambda^{*})$:

```{r, eval = FALSE, echo = TRUE}
hierarchical_prior_theta_sampler <- function(n, lambda) {
  indiv_mu <- rnorm(n = n, mean = lambda["pop_loc"], sd = lambda["pop_scale"])
  noise_sd <- rgamma(n = n, shape = lambda["noise_shape"], rate = lambda["noise_rate"])
  res <- tibble(x = c(indiv_mu, noise_sd), param = rep(c("mu", "sigma"), each = n))
  return(res)
}

theta_scale_plot_tbl <- bind_rows(
  pbbo_single_objective_res %>%
    get_best_lambda() %>%
    hierarchical_prior_theta_sampler(n = n_plot_samples, lambda = .) %>%
    mutate(obj = "single obj"),
  pbbo_multi_objective_res %>%
    get_best_lambda(pbbo_kappa = 0.75) %>%
    hierarchical_prior_theta_sampler(n = n_plot_samples, lambda = .) %>%
    mutate(obj = "multi obj")
)

ggplot(theta_scale_plot_tbl) +
  geom_line(
    mapping = aes(x = x, colour = obj), stat = "density", na.rm = FALSE, n = 5e4
  ) +
  labs(colour = "Type") +
  scale_colour_manual(
    values = c("multi obj" = pal_colours[1], "single obj" = pal_colours[2])
  ) +
  facet_grid(cols = vars(param)) +
  scale_y_continuous(limits = c(0, 4)) + 
  xlab("theta")
```

```{r}
knitr::include_graphics("plots/readme/hierarchical-theta.pdf")
```

we observe that the single objective approach attributes almost all the variation in the target to the prior for the noise. 
Such overconfidence is unsurprising as the total predictive discrepancy only optimisation problem is ill-posed. 
The multi-objective approach, and the regularisation it provides, estimates a more appropriate joint prior for the mean and noise parameters. 

## Tuning parameters

There are several tuning parameters to set when using `pbbo`.
These can be divided into parameters controlling the accuracy and evaluation cost of the discrepancy function, and parameters controlling the optimisation process.
Discrepancy related parameters include $S_{r}$, the number of samples drawn from the (covariate-specific) prior predictive distribution for a given value of $\lambda$, and $I_{r}$, the number of importance samples used to evaluate the corresponding total predictive discrepancy.
Optimisation related parameters include the number of CRS2 iterations to run in the first stage $N_{\text{CRS2}}$; the number of Bayesian optimisation batches to run $N_{\text{batch}}$, and the number of iterations within each batch $N_{\text{BO}}$; and the number of points to carry forward between each stage/batch to use in an initial design $N_{\text{design}}$.
The choice of importance density could also be considered a tuning parameter, but we do not explore optimal importance density approaches here.

Consider first the discrepancy parameters.
To investigate the effect of these parameters on the approximate total predictive discrepancy we construct a covariate-independent $\log(D(\lambda))$ using the covariate-independent target from Section \ref{a-human-aware-prior-for-a-human-growth-model}.
The particulars of this discrepancy and target are unimportant for our current purposes.
We note only that $\lambda$ is 10-dimensional and thus $\log(D(\lambda))$ remains noisy even for very large values of $S_{r}$ and $I_{r}$, and that $\log(D(\lambda))$ is heteroscedastic and thus we evaluate $\log(D(\lambda))$ for all choices of $S_{r}$ and $I_{r}$ at an optimal value $\lambda^{*}$, as the behaviour near the optimum is, perhaps, more important than for "poor" values of $\lambda$.
Consequently, we treat the following results as illustrative and they merely guide our choice of acceptable values for the tuning parameters.
Figure \ref{fig:tuning_params_discrep_params} displays the distribution of $\log(D(\lambda^{*}))$ for values of $I = I_{r}$ between $200 + 1$ and $10^4 + 1$; and values of $S = S_{r}$ between $200$ and $10^5$.
For each $I$ and $S$ we evaluate $\log(D(\lambda^{*}))$ 250 times to assess the residual noise in $D$.
It is evidently important for $S$ to be as large as computationally feasible.
The decrease in variability for a given value of $I$ is large, but the converse is not true.
We suggest as defaults $I = 5 \times 10^3$ and $S = 5 \times 10^{4}$, and generally recommend $I \ll S$ for settings where these defaults are computationally infeasible.

```{r tuning_params_discrep_params, fig.cap = "Distribution of $\\log(D(\\lambda^{*}))$, population target from the example in Section \\ref{a-human-aware-prior-for-a-human-growth-model}. Row panel titles denote the number of prior predictive samples $S$ used to form the ECDF $\\hat{\\Pd}(Y \\mid \\lambda, \\boldsymbol{y}^{(\\pd)})$, with column panel titles denoting the number of importance samples $I$ used to evaluate the integral. The red vertical line is the mean of the $S = 10^{5}, I = 10^{4} + 1$ (bottom right) case for reference." }
knitr::include_graphics("plots/tuning-parameters/variance-at-optima.pdf")
```

We now conduct a preliminary\footnote{More experiments on these tuning parameters are planned.} investigation into the optimisation parameters.
To assess and acquire appropriate minimum values for $N_{\text{CRS2}}, N_{\text{batch}}, N_{\text{BO}}$, and $N_{\text{design}}$, we consider each stage of the multi-stage optimisation process separately, and apply them solely to the total predictive discrepancy target.
We examine the covariate-independent $D(\lambda)$ from Section \ref{a-human-aware-prior-for-a-human-growth-model} and the previous experiment, as well as the corresponding covariate-dependent $D(\lambda \mid \boldsymbol{X})$, both with $S_{r} = 10^{5}$ and $I_{r} = 5 \times 10^{3}$.
CRS2 is run for 15 equally spaced values of $N_{\text{CRS2}}$ between and including 500 and 15000.
As a reference for the effect of batching, we run single batch $N_{\text{batch}} = 1$ Bayesian optimisation whilst varying $N_{\text{BO}}$ from 100 to 500 in 100 iteration increments.
In the multi-batch setting we consider values of $N_{\text{batch}}$ between $2$ and $10$ inclusively, each of $N_{\text{BO}} = 100, 200$, or $300$ iterations, and propagating $N_{\text{design}} =  25, 50, 75$, or $100$ points from the previous batch to the current batch as design points.
Each approach and setting combination is replicated 20 times to assess the noise in the values of $\lambda^{*}$ and thus $D(\lambda^{*})$ or $D(\lambda^{*} \mid \boldsymbol{X})$.

Figure \ref{fig:tuning_params_optim_params} displays $\log(D(\lambda))$ and $\log(D(\lambda \mid \boldsymbol{X}))$ at the final optimum $\lambda^{*}$ against runtime, for each of the approaches discussed in the previous paragraph.
Immediately apparent is the noise in the optima across all approaches, except perhaps the multi-batch approach with large numbers of batches and iterations.
All approaches struggle with the covariate-independent $\log(D(\lambda))$ objective, but CRS2 locates, on average, better optima in considerably less time.
More generally, the comparatively good and considerably faster performance of CRS2 is our justification for its use as stage one in our multi-stage optimisation approach.
The number of points propagated between batches (the rows of subpanel (B) in Figure \ref{fig:tuning_params_optim_params}) appears to minimally affect the result, but does increase run time, particularly for batches with a large number of iterations.

Whilst this experiment is by no means conclusive, we use it to guide our choices of $N_{\text{CRS2}}$, $N_{\text{BO}}$, $N_{\text{batch}}$, and $N_{\text{design}}$ in the forthcoming experiments, and suggest minimum values.
These minimums are $N_{\text{CRS2}} = 2000$, $N_{\text{BO}} = 250$ with $N_{\text{batch}} = 1$, and $N_{\text{design}} = \max(50, 4L)$ where $L$ is the dimension of $\lambda$.
Optimisation performance can then be improved most effectively, at some computational cost, by increasing $N_{\text{CRS2}}$ and/or $N_{\text{batch}}$.

```{r tuning_params_optim_params, fig.cap = "Optimal log total predictive discrepancies $\\log(D(\\lambda^{*}))$ against run time, for the optimisation approaches under numerous values of their respective tuning parameters. The top row (A) displays the single batch Bayesian optimisation approach, with the colour scale denoting $N_{\\text{BO}}$. The middle four rows (B) contain the multi-batch results, where the colour scale represents $N_{\\text{batch}} \\times N_{\\text{BO}}$. Note that this scale is only approximately related to the computational burden of the approach. Row titles in (B) denote $N_{\\text{design}}$. The bottom row (C) displays the CRS2 optima using $N_{\\text{CRS2}}$ iterations as denoted by the colour scale. The left and right columns contain the covariate-independent and covariate-specific total predictive discrepancies respectively. Note that the x-axis scale is logarithmic, and the blue line and uncertainty intervals come from a LOESS smooth using each panel's data. The red reference line is the median of the replicates of the most computationally expensive CRS2 case."}
knitr::include_graphics("plots/batching-and-optimiser-tests/fullpage-batching-tests-plot.pdf")
```

## General tips

We conclude this section with two practical tips for using `pbbo`.
Firstly, the prior distribution $\Pd(\theta \mid \lambda)$ should not admit a degenerate joint model for a value of $\lambda$ on the boundary of $\Lambda$.
The global optimisation methods typically require that $\Lambda$ be a closed and bounded subset of $\mathbb{R}^{L}$, and will try to evaluate the objectives at points on all boundaries.
If such a boundary point corresponds to degenerate prior, e.g. a point mass at zero for the marginal prior of a scale parameter, then $D(\lambda)$ and/or $N(\lambda)$ may be impossible to compute, or the latter may equal $-\infty$.
Secondly, although the loss in Equation \eqref{eqn:loss-definition} is appropriately defined for any $\kappa$, the results become increasingly degenerate, or at least over regularised, for values of $\kappa$ much larger than $1$.
Priors are insufficiently regularised for values of $\kappa$ too close to $0$.
We recommend assessing a variety of $\kappa$ values in between $0.1$ and $2$, but appropriate ranges for $\kappa$ will depend on the range of the secondary objective. -->