---
title: "Bayesian optimisation: possibly related/useful?"
author: "Andrew Manderson"
date: "`r format(Sys.time(), '%d %B, %Y')`"
fontfamily: tgpagella
fontsize: 10pt
papersize: a4
geometry: margin=2.25cm
bibliography: ../bibliography/prior-setting.bib
csl: ../bibliography/journal-of-the-royal-statistical-society.csl
output:
  pdf_document:
    includes:
      in_header:
        ../tex-input/pre.tex
    fig_caption: true
    number_sections: true
    keep_tex: true
---

<!--
# must be run from base dir, otherwise the incorrect library is loaded (not renv)
Rscript -e "rmarkdown::render('rmd-reports/2021-09-15_bayesian-optimisation-approach.rmd')"
-->

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = FALSE, comment = NA, fig.align = "center")

source('../scripts/common/setup-ggplot-theme.R')
```

As a reminder, we are interested in choosing the hyperparameters $\lambda$ of our prior predictive distribution $\pd(Y \mid \lambda, X)$ to make it as 'similar as possible' to some target predictive distribution $t(Y)$.
The key constraint is that we cannot evaluate $\pd(Y \mid \lambda, X)$ as we lack its analytic form.

# Bayesian optimisation

Bayesian optimisation is a fairly generic term for methods to optimise
\begin{equation}
  \min_{\lambda \in \Lambda} f(\lambda),
  \label{eqn:bayesian-opt-general}
\end{equation}
subject to the following:

1. The function $f(\lambda)$ is slow/expensive to evaluate, so we would like to limit the number of evaluations of $f(\lambda)$.
1. The gradient $\frac{\partial}{\partial \lambda}f(\lambda)$ is unavailable, eliminating common gradient-based optimisers (and approximating the gradient often requires a lot of evaluations of $f(\lambda)$ -- the situation we are trying avoid).
1. The parameter space $\Lambda$ is amenable to approximation via Gaussian process. Typically this means $\Lambda \subseteq \mathbb{R}^{d}$, for $d \leq 20$. This has implications for discrete hyperparameters, and other hyperparmeters that are not easily transformed to a (compact) subset of $\mathbb{R}^{d}$.
1. We observe $f(\lambda)$ with or without noise.

Bayesian optimisation proceeds to

1. Observe the function at a small number $k$ of initial design points $\{\lambda_{c}\}_{c = 1}^{k}$.
    - Often selected using a space-filling design, with some knowledge of the interval / subset of $\mathbb{R}$ that is relevant for each component of $\lambda$.
1. Form a GP approximation using the pairs of design points and evaluations $A_{k} = \{(\lambda_{c}, f(\lambda_{c}))\}_{c = 1}^{k}$.
    - Kernel selection and hyperparmeter optimisation are done in some context-specific way.
1. Select, via an _acquisition function_, an optimal value $\lambda_{k + 1}$ to evaluate the function at.
    - One particular acquisition function selects $\lambda_{k + 1}$ in the following way: denoting $\lambda^{*}$ as the current best value of $\lambda$, the next optimal $\lambda$ is
      \begin{equation}
        \lambda_{k + 1} = \min_{\lambda \in \Lambda} \text{E}_{k}[(f(\lambda) - f(\lambda^{*}))^{-}],
        \label{eqn:expected-improvement}
      \end{equation}
    where $\text{E}_{k}$ denote expectation w.r.t the GP formed using the initial $k$ points, and $(a)^{-} = \min(a, 0)$. This criteria is called the _expected improvement_ because typically one is concerned with _maximising_ \eqref{eqn:bayesian-opt-general}. The optimisation in \eqref{eqn:expected-improvement} is amenable to regular optimisation techniques because it is usually much cheaper to evaluate than the original $f(\lambda)$ (the expectation produces an analytic expression that is a function of the GP mean and kernel).
1. Reform (possibly re-estimating the hyperparameters) the GP using $A_{k + 1} = \{(\lambda_{k + 1}, f(\lambda_{k + 1}))\} \cup A_{k}$.
1. Repeat steps 3 and 4 until the budgeted number of evaluations $B$ of $f(\lambda)$ is reached, with each step using all previous function evaluations.
1. Return the minimum value of $f(\lambda)$ _predicted_ by the GP formed using all $B$ evaluations (not necessarily a point that has been previously evaluated).

This is my understanding after reading [@frazier_tutorial_2018].
The Bayesian optimisation literature seems large (multi-output GPs, many different acquisition strategies) and growing (as fast as the rest of ML?).

# What should $f$ be?

All the listed properties seem to be useful in our situation, however defining an appropriate $f(\lambda)$ is very challenging.
The $\alpha$-divergences [@minka_divergence_2005] assume you can evaluate something proportional to $\pd(Y \mid \lambda, X)$, and that the parameter you are trying to optimise is also the parameter you are integrating w.r.t to compute the divergence.
Neither of these is true in our setting.

A potential approach, for one dimensional problems, is to consider difference between the ECDF and the 'ideal' CDF.
Formally, consider a sample $\{y_{i}^{(\lambda)}\}_{i = 1}^{N}$ from $\pd(Y \mid \lambda, X)$, which is used to compute the ECDF $\widehat{F}^{(\lambda)}(z) = \frac{1}{N}\sum_{i = 1}^{N}\mathbbm{1}(z \leq y_{i})$.
Assume that $T(y) = \int t(u) \text{d}u$ is known or derivable.
We define a discrepancy function
\begin{equation}
  d(\lambda) = \int_{\mathcal{Y}} \|T(u) - \widehat{F}^{(\lambda)}(u)\|_{p} \text{d}u
  \label{eqn:discrep-def}
\end{equation}
for some $p$-norm.
This integral is intractable, so we must evaluate it numerically.
We adopt an importance sampling approach and write, for importance density $m(u)$ with appropriate support,
\begin{align}
  d(\lambda) &= \int_{\mathcal{Y}} \|T(u) - \widehat{F}^{(\lambda)}(u)\|_{p} \frac{m(u)}{m(u)} \text{d}u \\
  &= \text{E}_{m}\left[\frac{\|T(u) - \widehat{F}^{(\lambda)}(u)\|_{p}}{m(u)}\right] \\
  &\approx \sum_{j = 1}^{J} \frac{\|T(u_{j}) - \widehat{F}^{(\lambda)}(u_{j})\|_{p}}{m(u_{j})},
\end{align}
for samples $\{u_{j}\}_{j = 1}^{J}$ from $m(u)$.
Choosing $m(u)$ is not trivial, as we would like it to place more mass on points that contribute more towards the integral (i.e. have higher discrepancy).
One generic choice is to fit a mixture using the sample from $\pd(Y \mid \lambda, X)$ and an additional sample from $t(Y)$, both of which are easy to draw from.
If $\mathcal{Y}$ is a compact subset of $\mathbb{R}$ we can select $m(u)$ to be uniform over this compact subset.
To improve robustness we could use PSIS [@vehtari_pareto_2021].

# Simple 1D example

We will illustrate some further issues using the following example.
Define $t(Y)$ as the PDF of the random variable $Y \sim \text{Beta}(\alpha, \beta)$.
The first shape parameter, $\alpha$, will always be unknown; the second shape parameter, $\beta$, may be fixed for easier visualisation.
This setting has $\mathcal{Y} = [0, 1]$, and we use $U(0, 1)$ as the importance distribution so that $m(u) = 1$.

## Fixed $\beta = 8$

In this case $\lambda = \alpha$, which is is one-dimensional with support on the positive reals.
Using samples of size $N = 10^{2}$ from $\pd(Y \mid \lambda, X)$, we compare the discrepancy function for $p = 1, 2$.
We test this over 200 possible values for $\lambda = \alpha \in [2, 15]$.

```{r l1_l2_comparison, fig.height = 4, fig.cap = 'Loss/discrepancy for $p = 1$ (left column) and $p = 2$ (right column). The log loss/discrepancy is displayed in the top row, with the untransformed loss in the bottom row.'}
suppressPackageStartupMessages(library(dplyr))

plot_tbl <- readRDS('../rds/initial-beta-tests/l1-vs-l2-comparison.rds')

plot_tbl <- plot_tbl %>%
  mutate(
    plot_fact = factor(
      x = norm,
      levels = c('l1', 'l2'),
      labels = c(
        expression(italic(p) == 1),
        expression(italic(p) == 2)
      )
    )
  )

log_tbl <- plot_tbl %>%
  mutate(loss = log(loss), trans = 'log')

plot_tbl <- plot_tbl %>%
  mutate(trans = 'ident') %>%
  bind_rows(log_tbl) %>%
  mutate(
    plot_trans = factor(
      x = trans,
      levels = c('log', 'ident'),
      labels = c(
        expression(log(italic(d)(lambda))),
        expression(italic(d)(lambda))
      )
    )
  )

ggplot(plot_tbl, aes(x = lambda1, y = loss)) +
  geom_point() +
  facet_grid(
    cols = vars(plot_fact),
    rows = vars(plot_trans),
    labeller = label_parsed,
    scales = 'free_y'
  ) +
  xlab(expression(lambda == alpha)) +
  geom_vline(xintercept = 3, col = highlight_col, linetype = 'dashed') +
  theme(axis.title.y = element_blank())

```
Figure \ref{fig:l1_l2_comparison} displays all the aforementioned variants of the discrepancy function.
The minimum at $\alpha = 3$ is visible in both norms, however it is clearer (and would be easier to optimise for) in the $p = 2$ case (and would maybe agree with Section 6.4 of @gneiting_strictly_2007? Not sure I get the link between these ideas just yet).
One issue with the $p = 2$ case is that the constant variance assumption required by stationary GPs is violated.
Another is that $d(\lambda) \geq 0$ for all $\lambda \in \Lambda$, but we will not encode this information in the GP (doing so is hard).

## Unknown $\beta$

We use the Bayesian optimisation library `mlrMBO` [@bischl_mlrmbo_2018] to see how close we can get to the optimal parameters $\lambda = (\alpha, \beta) = (3, 8)$, with a budget of $B = 100$ iterations.
The initial $k$ points are selected using a spacing fill design, which requires constraining the components of $\lambda$ to compact intervals.
As such, we constrain $\alpha, \beta \in [0, 20]^{2}$.
For this test we set $p = 2$ and leave $d(\lambda)$ untransformed.

```{r mlrMBO_output, message = FALSE, fig.height = 6, fig.cap = 'The default plot from \\texttt{mlrMBO}, the fitted GP surface is visible in the top-left corner.'}
suppressPackageStartupMessages(library(mlrMBO))

res_obj <- readRDS('../rds/initial-beta-tests/mbo-2par-l2-100-iter-res.rds')

suppressWarnings(plot(res_obj))
```

Figure \ref{fig:mlrMBO_output} displays the default plot from `mlrMBO` (and is overly busy).
We can see the fitted surface in the top-left corner (here `lambda1` = $\alpha$ and `lambda1` = $\beta$).
The estimated parameters are $\hat{\alpha} =$ `r round(res_obj$x[[1]][1], 2)` and $\hat{\beta} =$ `r round(res_obj$x[[1]][2], 2)`,
and denote $\hat{\lambda} = (\hat{\alpha}, \hat{\beta})$.
These yield the a form of $\pd(Y \mid \hat{\lambda}, X)$ displayed in Figure \ref{fig:fit_check}, which is closer than I've ever gotten a prior predictive distribution to the shape I want it.


```{r fit_check, message = FALSE, fig.cap = 'The prior predictive using the optimal values $\\hat{\\lambda}$ (red), and the target prior predictive $t(Y)$.'}
lambda_vec <- res_obj$x[[1]]

ggplot() +
  stat_function(
    fun = dbeta,
    args = list(shape1 = 3, shape2 = 8),
    n = 500
  ) +
  stat_function(
    fun = dbeta,
    args = list(shape1 = lambda_vec[1], shape2 = lambda_vec[2]),
    col = 'red',
    n = 500
  ) +
  xlim(c(0, 1)) +
  xlab(expression(y)) +
  ylab(expression(p(y)))

```



## Summary of tuning parameters

1. $N$ -- The number of samples drawn from $\pd(Y \mid \lambda, X)$ used to estimate $F^{(\lambda)}(y)$.
1. $m(u)$ -- The importance distribution (and the number of importance samples $J$).
1. $p$ -- The norm used to compute the discrepancy.
1. $B$ -- The maximum number of values of $\lambda$ / evaluations of $d(\lambda)$ we allow ourselves (Note that because this is underpinned by a GP, setting too high a $B$ is a problem).

# Issues this doesn't address.

- Only really 1-D (how do you do regression problems here? What should the prior predictive look like for regression problems? It depends), though there are many problems where you have many individuals, each of which have a 1-d $Y$. In this case you can sum each individual's $d(\lambda)$.
- In this example, we know $\pd(Y \mid \lambda, X)$ can precisely fit $t(Y)$, but what about cases where $\pd(Y \mid \lambda, X)$ is more/less flexible than required to match $\pd(Y \mid \lambda, X)$?
- Only for unconstrained (or easily unconstrainable) spaces $\Lambda$. What about covariance/correlation matrices? Choosing them based on a lower-dim projection is really what I'm interested in.
  - Parameterise covariance matrices in terms of a diagonal variance matrix and the Cholesky of the correlation matrix, then use `Stan` to transform the entries in the Cholesky factor to an unconstrained space over which we can do Bayesian optimisation. (I'm not sure how amenable this space will be to approximation by GP? Can only try.)

# Bibliography