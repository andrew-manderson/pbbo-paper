---
title: "Setting priors by optimisation: initial idea"
author: "Andrew Manderson"
date: "`r format(Sys.time(), '%d %B, %Y')`"
fontfamily: tgpagella
fontsize: 10pt
papersize: a4
geometry: margin=2.25cm
bibliography: ../bibliography/prior-setting.bib
csl: ../bibliography/american-statistical-association.csl
output:
  pdf_document:
    includes:
      in_header:
        ../tex-input/pre.tex
    fig_caption: true
    number_sections: true
    keep_tex: true
---

<!--
Rscript -e "rmarkdown::render('2021-07-20_initial-idea.rmd')"
-->

# Motivation

Setting a prior for the model parameters can be hard when:

1. The prior information you have pertains to the prior predictive distribution,
1. The relationship between the parameters and the prior predictive is non-trival and/or non-linear,
1. The correlation between the parameters is crucial to ensuring that the prior predictive distribution is similar enough to you prior information (specifying correlation structures is extremely difficult),
1. The model (thus the prior) is conditional on covariates,
1. The support of the data/prior predictive constrained to an interval, where the interval bounds are themselves covariate / dependent on the data.


# Choosing prior hyperparameters by minimising the KL divergence

Say we are specifying a model for data $Y$ that has parameters $\theta$, covariates $X$ (which include other data-like information that the prior is conditional on), and we are trying to choose appropriate values for hyperparameters $\lambda$.
Our model is $\pd(Y, \theta \mid \lambda, X)$.
Assume that we can evaluate this density pointwise and draw simple Monte Carlo samples from it.
In many settings this will decompose to $\pd(Y \mid \theta, X) \pd(\theta \mid \lambda)$, but such assumptions are not yet required.

Suppose our prior information is in the form of a density function about the data $t(Y)$, which we can evaluate pointwise and draw samples from.
The prior predictive distribution for our model is
\begin{equation}
  \pd(Y \mid \lambda, X) = \int \pd(Y, \theta \mid \lambda, X) \text{d}\theta,
\end{equation}
which is typically intractable (though we can generate samples from it), and we cannot derive a pointwise expression that is proportional to it.
We would like to solve the following optimisation problem
\begin{equation}
  \min_{\lambda \in \Lambda} \kldiv{\pd(Y \mid \lambda, X)}{t(Y)} =
  \min_{\lambda \in \Lambda} \int \pd(Y \mid \lambda, X) \log\left(\frac{\pd(Y \mid \lambda, X)}{t(Y)}\right) \text{d}Y.
\end{equation}
There are many ways to rexpress this integral, but the central issue is that we don't have an expression (or anything proportional to) $\pd(Y \mid \lambda, X)$.
Converting the integrals into expectations yields
\begin{equation}
  \min_{\lambda \in \Lambda} \left\{
    \text{E}_{p}[\log(\pd(Y \mid \lambda, X))] - \text{E}_{p}[\log(t(Y))] %_
  \right\}.
\end{equation}
It is easy to compute the Monte Carlo approximation to the second expectation, but the first one is intractable.
Reversing the direction of the KL (switching the order of the arguments) gives
\begin{equation}
  \min_{\lambda \in \Lambda} \left\{
    \text{E}_{t}[\log(t(Y))] - \text{E}_{t}[\log(\pd(Y \mid \lambda, X))] %_
  \right\},
\end{equation}
of which the first term is constant, but again the second term is intractable.


## How does variational inference get around this problem?

The key property exploited by variational inferences is that the posterior is proportional to the joint, i.e. $\pd(\theta \mid Y) = \frac{1}{Z(Y)}\pd(Y, \theta)$ where the normalising constant does not depend on $\theta$.
This implies, for variational family $\q$ indexed by parameter $\lambda$
\begin{align}
  \min_{\lambda \in \Lambda} \kldiv{\q(\theta \mid \lambda)}{\pd(\theta \mid Y)}
  &= \min_{\lambda \in \Lambda} \int \q(\theta \mid \lambda) \log \left(\frac{\q(\theta \mid \lambda)}{\pd(\theta \mid Y)}\right) \text{d}\theta \\
  &= \min_{\lambda \in \Lambda} \int \q(\theta \mid \lambda) \log \left(\frac{\q(\theta \mid \lambda) Z(Y)}{\pd(\theta, Y)}\right) \text{d}\theta \\
  &= \min_{\lambda \in \Lambda} \left[
    \int \q(\theta \mid \lambda) \log\left(\frac{\q(\theta \mid \lambda)}{\pd(\theta, Y)}\right) \text{d}\theta
    + \int \q(\theta \mid \lambda) \log(Z(Y)) \text{d}\theta
  \right] \label{eqn:vi-kl-three} \\
  &= \min_{\lambda \in \Lambda} \left[
    \text{E}_{\q}\left[
      \log(\q(\theta \mid \lambda)) - \log(\pd(\theta, Y))
    \right]
    + \log(Z(Y))
  \right]. \label{eqn:vi-kl-four}
\end{align}
For a _fixed_ value of $Y$ (that is, a posteriori), $\log(Z(Y))$ is a constant, and so the minimum is achieved when the expectation in Equation \eqref{eqn:vi-kl-four} is minimised.
All the terms within the expectation can be evaluated pointwise.
Strategies for actually performing the minimisation are numerous (CAVI, ADVI, etc), it is a difficult optimisation problem.
Often the expectation is replaced with a Monte Carlo estimate, as it is typically easy to draw samples from $q(\theta \mid \lambda)$.

For our purpose it is important to note that

1. $Y$ is fixed in this setting, and it is not in ours
1. The latter integral in Equation \eqref{eqn:vi-kl-three} evaluated to $\log(Z(Y))$ because the integral was over $\theta$ and not $Y$.

## Options

1. Numerically integrate $\int \pd(Y, \theta \mid \lambda, X) \text{d}\theta$. This would need re-doing for each value of $\lambda$ and $Y$, and $\theta$ is usually of moderate or large dimension so the accuracy of the integration may be insufficient.
1. Another problem is that we'll probably need a gradient-based method to do the minimisation? (the conditionals necessary for CAVI will almost certainly be intractable). This leads us to needing
\begin{equation}
  \frac{\partial}{\partial \lambda} \text{E}_{t}\left[\log(\pd(Y \mid \lambda, X))\right] =
  \frac{\partial}{\partial \lambda} \int \log \left\{
    \int \pd(Y, \theta \mid \lambda, X) \text{d}\theta
  \right\} t(Y) \text{d}Y
\end{equation}
and I can't yet see how one computes this gradient (adjoint? Not linear). Maybe one of gradient free / finite difference Nesterov schemes could work.

# Other literature

## History matching and ABC to choose prior hyperparameters

The most relevant paper seems to be @wang_using_2018, who have the same motivation as we do (The ABC element of the methodology seems to be shared with @nott_approximation_2018-1).

Consider the model $\pd(Y, \theta \mid \lambda, X)$ and assume it decomposes such that $\pd(Y \mid \theta, \lambda, X) \pd(\theta \mid \lambda, X)$.
The problem is to choose a value of $\lambda$ such that $\pd(\theta, \lambda, X)$ encodes the prior information available, which pertains to some properties of the prior predictive distribution $\pd(Y \mid \lambda, X)$.
Assume that summary statistics $S^{j} = S^{j}(Y)$ for $j = 1, \ldots, J$ are available.
These are things like the mean and variance of the prior (simulated) data, and might be more sophisticated, e.g. the proportion of points that lie within some feasible set.
Suppose there is prior information about plausible and implausible values for each summary statistics.
Specifically, suppose that we have $B_{I}^{j}$ _implausible_ values of $S^{j}$ denoted $h_{I, b}^{j}$ for $b = 1, \ldots, B_{I}^{j}$, and $B_{P}^{j}$ _plausible_ values $h_{P, b}^{j}$ for $b = 1, \ldots, B_{P}^{j}$.

Let $\pd(S^{j}(Y) \mid \lambda, X)$ be the prior predictive distribution for the summary statistic under the prior $\pd(\theta \mid \lambda, X)$, i.e.
\begin{equation}
  \pd(S^{j}(Y) \mid \lambda, X) =
  \int
    \pd(S^{j}(Y) \mid \theta, \lambda, X)
    \pd(\theta \mid \lambda, X)
  \text{d}\theta
\end{equation}
with a slight abuse of notation because it's not actually clear what $\pd(S^{j}(Y) \mid \theta, \lambda, X)$ is, but practically one can draw from it by sampling $Y$ and computing $S^{j}(Y)$.
The analytic form of $\pd(S^{j}(Y) \mid \lambda, X)$ is unavailable.
Wang _et.&nbsp;al._&nbsp;consider the following p-value for the implausible points (suppressing the dependence on $X$ because it gets a little hairy otherwise)
\begin{equation}
  \rho_{I, b}^{j} = \Pr(\log \pd(S^{j}(Y) \mid \lambda) \leq \log \pd(S^{j}(Y) = h_{I, b}^{j} \mid \lambda)),
\end{equation}
and
\begin{equation}
  \rho_{P, b}^{j} = \Pr(\log \pd(S^{j}(Y) \mid \lambda) \leq \log \pd(S^{j}(Y) = h_{P, b}^{j} \mid \lambda)),
\end{equation}
for the plausible points.
I find these a little hard to understand, but I think the log, and thus reversal of the inequality from the usual p-value definition, is because they care more about the tail behaviour?
Whilst these are hard to calculate, these allow Wang _et.&nbsp;al._&nbsp; to define an acceptable prior (and thus value of $\lambda$) as one a prior with
$\rho_{I, b}^{j} < \alpha$ (for all $h_{I, b}^{j}$) and $\rho_{P, b}^{j} \geq \alpha$ (for all $h_{P, b}^{j}$) for a given threshold $\alpha \in [0, 1]$.
That is to say, an acceptable prior is one such that all the implausible points are sufficiently surprising under the prior, whilst the plausible points are not surprising.

The p-values are then used to define an _implausibility measure_
\begin{equation}
  I(\lambda) =
    \sum_{j = 1}^{J}\sum_{b = 1}^{B_{I}^{j}} \max(0, \rho_{I, b}^{j} - \alpha) +
    \sum_{j = 1}^{J}\sum_{b = 1}^{B_{P}^{j}} \max(0, \alpha - \rho_{P, b}^{j})
    \label{eqn:implausibility-measure}
\end{equation}
which one can minimise to find a permissible value for $\lambda$ -- values that lead to exact zeros to Equation \eqref{eqn:implausibility-measure} would be ideal but are not necessary.

They then employ a number of methods to make this idea work:

1. They use an ABC regression approach to approximate $\pd(S^{j}(Y) \mid \theta, \lambda, X)$ which makes sampling the data (given samples of values for $\theta$) much faster, so that they can search a large space of values for $\lambda$.
1. They use history matching as a technique for exploring the space of possible $\lambda \in \Lambda$. This is necessary as gradient based approaches to optimising \eqref{eqn:implausibility-measure} seem impossible (I have no idea how you get the gradient w.r.t $\lambda$ there).

My critique would be that the method is limited by the use of summary statistics and typically small numbers of plausible / implausible values (in their examples there are usually 1 or 2 of each).
Making use of the whole shape / information encoded in $t(Y)$ would be nice (but seems hard).

## Penalized complexity priors

Another relevant idea is the penalised complexity (PC) prior of @simpson_penalising_2017 (and the discussants of that particular paper)