# Translating elicited prior predictive distributions

## The target distribution

## Minimum discrepancy objective (primary objective)

- cut second {X_{r}}
- Integral is w.r.t to d\tc(Y).
- Quick sentence on covariate-independent and its relationship to (1)
- Switch to P_{1} and P_{2}, because letter are a bit confusing tbh.



## Stabilising the estimates by promoting the marginal variance (secondary objective)

- RS for (3)
- F => P for just after (4).

- Assumption in (4) is that SD_{F} exists, if this is not the case, we can use some other measure of scale/variability that is well behaved for distributions with no second moment. (Hodges-Lehmann type, IQR, rank-based measurements of variability) 

## Post-optimisation process

- log scale thing? Or point out earlier. (point out def of (4) makes log values more sensible?)

## Estimation strategy

- With out objectives defined, we now introduce our estimation strategy.
- Borrow a lot from current start of Section 2 (make multi-objective clearer.)
- re use faithfulness (stage 1) and uniqueness/replicabilty (stage 2).


- Overall introduction to the strategy
- Pictorial representation of the process

### Evaluating the objectives (objective evaluation)

- Before optimisation can occur, we must be able to practically evaluate all the objectives.
- Evaluating $N(\lambda)$ is usually straightforward. In most models we have analytic expressions for $SD_{\Pd}[\theta]$, but estimates would also be simple to compute using Monte Carlo samples.
- However, evaluating $D(\lambda)$ is challenging.

- We make two approximations to evaluate (1): we approximate the analytically unknown CDF $\Pd()$ using its empirical ECDF (ECDF), and evaluate the integrals in Equation (1) using importance sampling.
- Specifically, given a specific value for $\lambda$ we sample $S_{r}$ samples from $\Pd(Y \mid \lambda, X_{r})$ and form the 

- Notation $\tilde$ to no $\tilde$

- Bring forward some importance sampling information (maths etc).
- Leave support-based choice of Q to appendix.
- Note that we use the sample that is used to from the ECDF to pick an importance distribution (an likewise a sample from \tc(Y)).

- Make a table with our examples $\mathcal{Y}$ and the choice of importance density for each of them.

#### Optimisation, stage 1

- Only focus on faithfulness
- Focus is not only on finding an optimum value of $\lambda$, but finding a whole design / series of points to initialise the second stage.
- CRS2 algorithm(s)

#### Optimisation, stage 2

- Bayesian optimisation, multiple objectives (here only two)
- Typically, for the uniqueness reasons aforementioned, this is multi-objective.
- End with MSPOT algorithm

- Batching description, why it is necessary

- (breifly) it can be a single objective stage (and probably unlikely to add a great deal of additional value once finished with stage one).

### Summary

- BP list of requirements (add tuning parameters)
- Big picture algorithm


------- ##### other

- population => covariate-independent
- Switch to Roman T/t for target I think. (use macros)
